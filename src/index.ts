type Wager = {horse:string, wager:number};
type Finish = {horseName:string, time:number};

//класс игроков
class Player{
  playerWagers: Wager[];
  name:string;
  account:number;
  constructor( name:string, account:number){
    this.name = name;
    this.account = account;
    this.playerWagers =[];
  }
}

//новая игра
let player:Player;
function newGame(name:string, account:number = 100) {
  player = new Player(name, account);
}

//класс лошадей
class Horse {
  horseName:string;
  constructor(horseName:string) {
    this.horseName = horseName;
  }
  run ():Promise<Finish> {
    return new Promise(resolve => {
      const timeRandom:number = Math.floor(500 + Math.random() * (3000 + 1 - 500));
      setTimeout(() => {
        resolve({
          horseName: this.horseName,
          time: timeRandom
        });
      }, timeRandom)
    })
  }
}

const horses = [
  new Horse('Голубая'),
  new Horse('Желтая'),
  new Horse('Красная'),
  new Horse('Розовая'),
  new Horse('Зеленая')
];

//выводит список лошадей
function showHorses():void {
  horses.forEach(item => {
    console.log(item.horseName);
  });
}
//выводит счет игрока
function showAccount():void {
  console.log(`Счет игрока ${player.name} равен ${player.account}`);
}

//устанавливает ставку на лошадь в следующем забеге
function setWager(horseName:string, wager:number) {
  if (player.account - wager < 0) {
    throw new Error("У вас недостаточно денег");
  }
  player.account -= wager;
  player.playerWagers.push({horse: horseName, wager: wager});
}

//дает старт скачкам
function startRacing():void {
  let winner:string;
  const races: Promise<Finish>[] = [];
  horses.forEach((item) =>{
    races.push(item.run())
  });
  Promise.race(races)
      .then(winnerIs => {
        winner = winnerIs.horseName;
      }).catch(() => {
    console.log('Что-то пошло не так');
  });
  Promise.all(races)
      .then(values => {
        values.forEach(item => {
          console.log(`${item.horseName} -- ${item.time}`);
        });
        update();
      })
      .catch(() => {
        console.log('Что-то пошло не так');
      });

    function update():void {
    let accountNew:number = 0;
    console.log(`В забеге выигрывает ${winner}`);
    for (let item of player.playerWagers) {
      if (item.horse === winner) {
        accountNew = item.wager * 2;
        player.account += accountNew;
      }
    }
    console.log(`Вы выиграли ${accountNew || 0}, текущий счет ${player.account}`);
    player.playerWagers = [];
  }
}

showHorses();
newGame('Nastya', 300);
showAccount();
setWager('Красная', 20);
setWager('Голубая', 90);
startRacing();
